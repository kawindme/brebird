import numpy as np


def double_gauß(x, phi, std):
    gauß1 = 1/(2 * np.pi * std**2) * np.exp(- (x - phi)**2 / (2*std**2))
    gauß2 = 1/(2 * np.pi * std**2) * np.exp(- (x - (phi-360))**2 / (2*std**2))
    return gauß1 + gauß2

def alpha_func(x, phi=360, std=20):
    return double_gauß(x, phi, std) / double_gauß(phi, phi, std)

if __name__ == "__main__":
    
    import matplotlib.pyplot as plt
    
    x = np.linspace(360, 0, 50)
    
    plt.plot(x, alpha_func(x))
