import numpy as np

from bokeh.models import ColumnDataSource
from bokeh.plotting import figure, show, curdoc
from bokeh.models import HoverTool, Span
from bokeh.models.widgets import (Slider, DataTable, TableColumn, 
                                  NumberEditor, Div)
from bokeh.layouts import column, layout

from linkage import calculate_linkage5 as calculate_linkage
from linkage import get_lines5 as get_lines
from circle_intersection import SolutionError

from read_Gatesy93 import x_means as x_designs
from read_Gatesy93 import y_means as y_designs
from alpha_func import alpha_func

def points_to_xy(points):
    x = np.empty(len(points))
    y = np.empty_like(x)
    for i, p in enumerate(points):
        x[i] = p.x
        y[i] = p.y
    return x, y

def lines_to_xy(lines):
    x_parts = []
    y_parts = []
    for line in lines:
        # bokeh starts new line after np.nan
        x = np.array([line.x1, line.x2, np.nan]) 
        y = np.array([line.y1, line.y2, np.nan])
        x_parts.append(x)
        y_parts.append(y)
    xs = np.concatenate(x_parts)
    ys = np.concatenate(y_parts)
    return xs, ys


#A_x = 20
#A_y = 0
#D_x = 79.2
#D_y = 57.6
#d_AB = 12.5
#d_AF = 12.5
#d_BC = 64
#d_CD = 40
#d_CE = 104
#d_EG = 40
#d_EH = 112
#d_FG = 136

phi = 0

A_x, A_y = (0, 0)
F_x, F_y = (0, 50)
d_AB, d_BC, d_BE, d_CD = (22.5, 30, 50, 5)
d_EG, d_FG, d_GH, d_HI = (22, 10, 10, 55)
d_IJ, d_IK, d_IL, d_JK = (25, 25, 279, 25)
d_DJ = 84

var_names = ["A_x", "A_y", "F_x", "F_y", 
             "d_AB", "d_BC", "d_BE", "d_CD", 
             "d_EG", "d_FG", "d_GH", "d_HI",
             "d_IJ", "d_IK", "d_IL", "d_JK",
             "d_DJ"]

var_values = [0, 0, 0, 50,
              22.5, 30, 50, 5, 
              22, 10, 10, 55,
              25, 25, 279, 25,
              84]


(A_x, A_y, F_x, F_y, 
 d_AB, d_BC, d_BE, d_CD, 
 d_EG, d_FG, d_GH, d_HI,
 d_IJ, d_IK, d_IL, d_JK,
 d_DJ) = var_values

var_source = ColumnDataSource({"name": var_names, "value": var_values})

columns = [
    TableColumn(field="name", title="Variable Name"),
    TableColumn(field="value", title="Value", editor=NumberEditor()),
]

data_table = DataTable(source=var_source, columns=columns, width=200, 
                       height=600, editable=True)



points = calculate_linkage(phi, A_x, A_y, F_x, F_y, 
                           d_AB, d_BC, d_BE, d_CD, 
                           d_EG, d_FG, d_GH, d_HI,
                           d_IJ, d_IK, d_IL, d_JK,
                           d_DJ)
lines = get_lines(points)

points_x, points_y = points_to_xy(points) 
lines_x, lines_y = lines_to_xy(lines)

point_names = [p.name for p in points]
line_names = [l.name for l in lines]
line_names_for_source = sum([[ln] * 3 for ln in line_names], [])

point_data = {"points_x": points_x, "points_y": points_y, 
              "names": point_names}
line_data = {"lines_x": lines_x, "lines_y": lines_y,
             "names": line_names_for_source}
point_source = ColumnDataSource(point_data)
line_source = ColumnDataSource(line_data)


p = figure(match_aspect=True, 
           x_range=(-100, 400), 
           y_range=(-250, 250)) 
line_plot = p.line("lines_x", "lines_y", source=line_source, color="black")
p.add_tools(HoverTool(renderers=[line_plot], tooltips=[('Line',"@names")]))
point_plot = p.scatter("points_x", "points_y", source=point_source, color="black")
p.add_tools(HoverTool(renderers=[point_plot], tooltips=[('Point',"@names")]))

phi_slider = Slider(start=0, end=360, value=0, step=1, direction="ltr", title="Phi")
offset_slider = Slider(start=-180, end=180, value=0, step=1, title="Offset")
status_text = Div(text="Status: OK")

alphas = [alpha_func(phi_slider.value, deg) for deg in range(0, 360, 60)]
pigeon_neck_x = min([F_x , A_x]) - max([np.sqrt(d_FG**2 + d_EG**2), d_AB])
pigeon_neck_y = F_y + d_FG
design_data = {"design_x": x_designs - abs(pigeon_neck_x), 
               "design_y": y_designs + pigeon_neck_y,
               "alpha": alphas}
design_source = ColumnDataSource(design_data)
design_plot = p.scatter("design_x", "design_y", source=design_source,
                        line_alpha="alpha", size=40, line_color="green",
                        fill_alpha=0, line_width=5)



def update(attr, old, new):
    phi = phi_slider.value + offset_slider.value
    (A_x, A_y, F_x, F_y, 
     d_AB, d_BC, d_BE, d_CD, 
     d_EG, d_FG, d_GH, d_HI,
     d_IJ, d_IK, d_IL, d_JK,
     d_DJ) = var_source.data["value"]
    status_text.text = "Status: OK"
    try:
        points = calculate_linkage(phi, A_x, A_y, F_x, F_y, 
                                   d_AB, d_BC, d_BE, d_CD, 
                                   d_EG, d_FG, d_GH, d_HI,
                                   d_IJ, d_IK, d_IL, d_JK,
                                   d_DJ)
    except SolutionError as e:
        status_text.text = "Error: {}".format(e)
        return
    lines = get_lines(points)
    points_x, points_y = points_to_xy(points) 
    lines_x, lines_y = lines_to_xy(lines)
    point_data = {"points_x": points_x, "points_y": points_y, 
                  "names": point_names}
    line_data = {"lines_x": lines_x, "lines_y": lines_y,
                 "names": line_names_for_source}
    point_source.data = point_data
    line_source.data = line_data
    
    alphas = [alpha_func(phi_slider.value, deg) for deg in range(0, 360, 60)]
    pigeon_neck_x = min([F_x , A_x]) - max([np.sqrt(d_FG**2 + d_EG**2), d_AB])
    pigeon_neck_y = F_y + d_FG
    design_data = {"design_x": x_designs - abs(pigeon_neck_x), 
                   "design_y": y_designs + pigeon_neck_y,
                   "alpha": alphas}
    design_source.data = design_data
    
offset_slider.on_change('value', update)
phi_slider.on_change('value', update)
data_table.source.on_change('data', update)


l = layout([[column(status_text, data_table), 
                    column([p, offset_slider, phi_slider])]], 
            )

#show(l)
curdoc().add_root(l)