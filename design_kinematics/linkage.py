import math

import numpy as np
import pandas as pd

from geometries import Point, Circle, Line, Vector
from circle_intersection import (calculate_circle_intersections, 
                                 SolutionError)

# %%
def calculate_linkage(phi, A_x, A_y, D_x, D_y, 
                      d_AB, d_BC, d_BF, d_CD, 
                      d_CE, d_EG, d_EH, d_FG):
    A = Point(A_x, A_y, name="A")
    D = Point(D_x, D_y, name="D")
    # B
    B = Point(A.x + d_AB * math.cos(math.radians(phi)), 
              A.y + d_AB * math.sin(math.radians(phi)),
              name="B"
              )
    # C
    circ_B = Circle(B, d_BC)
    circ_D = Circle(D, d_CD)
    C1, C2 = calculate_circle_intersections(circ_B, circ_D)
    if C1.y > C2.y:
        C = C1
    else:
        C = C2
    C.name = "C"
    # E
    v_CD = D - C
    v_CE = v_CD.to_unit() * d_CE
    E_vec = C + v_CE
    E = Point(E_vec.x, E_vec.y, name="E")
    # F
    v_BC = C - B
    v_BF = v_BC.to_unit() * d_BF
    F_vec = B + v_BF
    F = Point(F_vec.x, F_vec.y, name="F")
    # G
    circ_E = Circle(E, d_EG)
    circ_F = Circle(F, d_FG)
    G1, G2 = calculate_circle_intersections(circ_E, circ_F)
    G1.name = "G1"
    G2.name = "G2"
    if G1.y < G2.y:
        G = G1
    else:
        G = G2
    G.name = "G"
    # H
    v_EG = G - E
    v_EH= v_EG.to_unit() * d_EH
    H_vec = E + v_EH
    H = Point(H_vec.x, H_vec.y, name="H")
    
    result = [A, B, C, D, E, F, G, H]

    return result


def calculate_linkage2(phi, A_x, A_y, D_x, D_y, 
                      d_AB, d_AF, d_BC, d_CD, 
                      d_DE, d_EG, d_EH, d_FG):
    A = Point(A_x, A_y, name="A")
    D = Point(D_x, D_y, name="D")
    # B
    B = Point(A.x + d_AB * math.cos(math.radians(phi)), 
              A.y + d_AB * math.sin(math.radians(phi)),
              name="B"
              )
    # C
    circ_B = Circle(B, d_BC)
    circ_D = Circle(D, d_CD)
    C1, C2 = calculate_circle_intersections(circ_B, circ_D)
#    if C1.y > C2.y:
#        C = C1
#    else:
#        C = C2
    if C1.y > C2.y and C1.x < D.x:
        C = C1
    else:
        C = C2
    C.name = "C"
    # E
    v_CD = D - C
    v_DE = v_CD.to_unit() * d_DE
    E_vec = D + v_DE
    E = Point(E_vec.x, E_vec.y, name="E")
    
    # F
    F = Point(A.x + d_AF * math.cos(math.radians(phi + 180)), 
              A.y + d_AF * math.sin(math.radians(phi + 180)),
              name="F"
              )
#    v_BC = C - B
#    v_BF = v_BC.to_unit() * d_BF
#    F_vec = B + v_BF
#    F = Point(F_vec.x, F_vec.y, name="F")
    
    # G
    circ_E = Circle(E, d_EG)
    circ_F = Circle(F, d_FG)
    G1, G2 = calculate_circle_intersections(circ_E, circ_F)
    G1.name = "G1"
    G2.name = "G2"
    if G1.y < G2.y:
        G = G1
    else:
        G = G2
    G.name = "G"
    # H
    v_EG = G - E
    v_EH= v_EG.to_unit() * d_EH
    H_vec = E + v_EH
    H = Point(H_vec.x, H_vec.y, name="H")
    
    result = [A, B, C, D, E, F, G, H]

    return result

def get_lines2(points):
    A, B, C, D, E, F, G, H = points
    v_AB = B - A
    v_AF = F - A
    v_BC = C - B
    v_CE = E - C
    v_FG = G - F
    v_EH = H - E
    l_AB = Line(A, v_AB, "AB")
    l_AF = Line(A, v_AF, "AF")
    l_BC = Line(B, v_BC, "BC")
    l_CE = Line(C, v_CE, "CE")
    l_FG = Line(F, v_FG, "FG")
    l_EH = Line(E, v_EH, "EH")
    return [l_AB, l_AF, l_BC, l_CE, l_FG, l_EH]


def calculate_linkage3(phi, A_x, A_y, D_x, D_y, 
                      d_AB, d_AF, d_BC, d_CD, 
                      d_DE, d_EG, d_EH, d_FI,
                      d_GI):
    A = Point(A_x, A_y, name="A")
    D = Point(D_x, D_y, name="D")
    # B
    B = Point(A.x + d_AB * math.cos(math.radians(phi)), 
              A.y + d_AB * math.sin(math.radians(phi)),
              name="B"
              )
    # C
    circ_B = Circle(B, d_BC)
    circ_D = Circle(D, d_CD)
    try:
        C1, C2 = calculate_circle_intersections(circ_B, circ_D)
    except SolutionError as e:
        raise SolutionError("Error for Point C: " + str(e)) from e

    if C1.y > C2.y and C1.x < D.x:
        C = C1
    else:
        C = C2
    C.name = "C"
    # E
    v_CD = D - C
    v_DE = v_CD.to_unit() * d_DE
    E_vec = D + v_DE
    E = Point(E_vec.x, E_vec.y, name="E")
    
    # F
    F = Point(A.x + d_AF * math.cos(math.radians(phi + 180)), 
              A.y + d_AF * math.sin(math.radians(phi + 180)),
              name="F"
              )
#    v_BC = C - B
#    v_BF = v_BC.to_unit() * d_BF
#    F_vec = B + v_BF
#    F = Point(F_vec.x, F_vec.y, name="F")
    
    # I
    d_EI = math.sqrt(d_EG**2 + d_GI**2)
    circ_E_I = Circle(E, d_EI)
    circ_F_I = Circle(F, d_FI)
    try:
        I1, I2 = calculate_circle_intersections(circ_E_I, circ_F_I)
    except SolutionError as e:
        raise SolutionError("Error for Point I: " + str(e)) from e
    I1.name = "I1"
    I2.name = "I2"
    if I1.y < I2.y:
        I = I1
    else:
        I = I2
    I.name = "I"
    
    # G
    circ_E_G = Circle(E, d_EG)
    circ_I_G = Circle(I, d_GI)
    try:
        G1, G2 = calculate_circle_intersections(circ_E_G, circ_I_G)
    except SolutionError as e:
        raise SolutionError("Error for Point G: " + str(e)) from e
    G1.name = "G1"
    G2.name = "G2"
    if G1.y > G2.y:
        G = G1
    else:
        G = G2
    if G.y < E.y:
        if G1.x > G2.x:
            G = G1
        else:
            G = G2
    G.name = "G"
    
    # H
    v_EG = G - E
    v_EH= v_EG.to_unit() * d_EH
    H_vec = E + v_EH
    H = Point(H_vec.x, H_vec.y, name="H")
    
    
    result = [A, B, C, D, E, F, G, H, I]

    return result

def get_lines3(points):
    A, B, C, D, E, F, G, H, I = points
    v_AB = B - A
    v_AF = F - A
    v_BC = C - B
    v_CE = E - C
    v_EH = H - E
    v_FI = I - F
    v_GI = I - G
    l_AB = Line(A, v_AB, "AB")
    l_AF = Line(A, v_AF, "AF")
    l_BC = Line(B, v_BC, "BC")
    l_CE = Line(C, v_CE, "CE")
    l_EH = Line(E, v_EH, "EH")
    l_FI = Line(F, v_FI, "FI")
    l_GI = Line(G, v_GI, "GI")
    return [l_AB, l_AF, l_BC, l_CE, l_EH, l_FI, l_GI]

def calculate_linkage4(phi, A_x, A_y, D_x, D_y, 
                      d_AB, d_AF, d_BC, d_CD, 
                      d_DE, d_EG, d_EH, d_FI,
                      d_GI, a_F):
    A = Point(A_x, A_y, name="A")
    D = Point(D_x, D_y, name="D")
    # B
    B = Point(A.x + d_AB * math.cos(math.radians(phi)), 
              A.y + d_AB * math.sin(math.radians(phi)),
              name="B"
              )
    # C
    circ_B = Circle(B, d_BC)
    circ_D = Circle(D, d_CD)
    try:
        C1, C2 = calculate_circle_intersections(circ_B, circ_D)
    except SolutionError as e:
        raise SolutionError("Error for Point C: " + str(e)) from e

    if C1.y > C2.y and C1.x < D.x:
        C = C1
    else:
        C = C2
    C.name = "C"
    # E
    v_CD = D - C
    v_DE = v_CD.to_unit() * d_DE
    E_vec = D + v_DE
    E = Point(E_vec.x, E_vec.y, name="E")
    
    # F
    F = Point(A.x + d_AF * math.cos(math.radians(phi + a_F)), 
              A.y + d_AF * math.sin(math.radians(phi + a_F)),
              name="F"
              )
    
    # I
    d_EI = math.sqrt(d_EG**2 + d_GI**2)
    circ_E_I = Circle(E, d_EI)
    circ_F_I = Circle(F, d_FI)
    try:
        I1, I2 = calculate_circle_intersections(circ_E_I, circ_F_I)
    except SolutionError as e:
        raise SolutionError("Error for Point I: " + str(e)) from e
    I1.name = "I1"
    I2.name = "I2"
    if I1.y < I2.y and not I1.x < D.x:
        I = I1
    else:
        I = I2
    I.name = "I"
    
    # G
    circ_E_G = Circle(E, d_EG)
    circ_I_G = Circle(I, d_GI)
    try:
        G1, G2 = calculate_circle_intersections(circ_E_G, circ_I_G)
    except SolutionError as e:
        raise SolutionError("Error for Point G: " + str(e)) from e
    G1.name = "G1"
    G2.name = "G2"
    if G1.y > G2.y:
        G = G1
    else:
        G = G2
    if G.y < E.y:
        if G1.x > G2.x:
            G = G1
        else:
            G = G2
    G.name = "G"
    
    # H
    v_EG = G - E
    v_EH= v_EG.to_unit() * d_EH
    H_vec = E + v_EH
    H = Point(H_vec.x, H_vec.y, name="H")
    
    
    result = [A, B, C, D, E, F, G, H, I]

    return result

def get_lines4(points):
    A, B, C, D, E, F, G, H, I = points
    v_AB = B - A
    v_AF = F - A
    v_BC = C - B
    v_CE = E - C
    v_EH = H - E
    v_FI = I - F
    v_GI = I - G
    l_AB = Line(A, v_AB, "AB")
    l_AF = Line(A, v_AF, "AF")
    l_BC = Line(B, v_BC, "BC")
    l_CE = Line(C, v_CE, "CE")
    l_EH = Line(E, v_EH, "EH")
    l_FI = Line(F, v_FI, "FI")
    l_GI = Line(G, v_GI, "GI")
    return [l_AB, l_AF, l_BC, l_CE, l_EH, l_FI, l_GI]


def calculate_linkage5(phi, A_x, A_y, F_x, F_y,
                       d_AB, d_BC, d_BE, d_CD,
                       d_EG, d_FG, d_GH, d_HI,
                       d_IJ, d_IK, d_IL, d_JK,
                       d_DJ):

    A = Point(A_x, A_y, name="A")
    F = Point(F_x, F_y, name="F")
    
    # B
    B = Point(A.x + d_AB * math.cos(math.radians(phi)), 
              A.y + d_AB * math.sin(math.radians(phi)),
              name="B")
    
    # E
    d_EF = math.sqrt(d_EG**2 + d_FG**2)
    circ_B = Circle(B, d_BE)
    circ_F = Circle(F, d_EF)
    
    E1, E2 = calculate_circle_intersections(circ_B, circ_F)
    E1.name = "E1"
    E2.name = "E2"
    
    
    if E1.x < E2.x:
        E = E1
    else:
        E = E2
    E.name = "E"
    
    
    # C
    v_BE = E - B
    vec_C = B + v_BE.to_unit() * d_BC
    C = Point(vec_C.x, vec_C.y, name="C")
    
    # D
    unit_BC = v_BE.to_unit()
    # -90° rotation
    v_CD = Vector(unit_BC.y, -unit_BC.x) * d_CD
    vec_D = C + v_CD
    D = Point(vec_D.x, vec_D.y, name="D")
    
    # G
    eta = math.atan(d_FG / d_EG) # verändert sich nicht während bewegung, muss nicht immer wieder berechnet werden
    rot_eta = np.array([[math.cos(eta), -math.sin(eta)],
                        [math.sin(eta), math.cos(eta)]])
    unit_EF = (F - E).to_unit()
    arr_EG = np.matmul(rot_eta, np.array([[unit_EF.x],[unit_EF.y]])) * d_EG
    v_EG = Vector(arr_EG[0,0], arr_EG[1,0])
    vec_G = E + v_EG
    G = Point(vec_G.x, vec_G.y, name="G")
    
    
    # H
    v_GH = v_EG.to_unit() * d_GH
    vec_H = G + v_GH
    H = Point(vec_H.x, vec_H.y, name="H")
    
    # I 
    v_HI = v_GH.to_unit() * d_HI
    vec_I = H + v_HI
    I = Point(vec_I.x, vec_I.y, name="I")
    
    # J
    circ_D = Circle(D, d_DJ)
    circ_I = Circle(I, d_IJ)
    
    J1, J2 = calculate_circle_intersections(circ_D, circ_I)
    J1.name = "J1"
    J2.name = "J2"
    
    if J1.x > J2.x or J2.y > I.y:
        J = J1
    else:
        J = J2
    J.name = "J"
    
    # K
    gamma = math.acos((d_IJ**2 + d_IK**2 - d_JK**2) / (2 * d_IJ * d_IK))
    rot_gamma = np.array([[math.cos(gamma), -math.sin(gamma)],
                          [math.sin(gamma), math.cos(gamma)]])
    unit_IJ = (J - I).to_unit()
    arr_IK = np.matmul(rot_gamma, np.array([[unit_IJ.x],[unit_IJ.y]])) * d_IK
    v_IK = Vector(arr_IK[0,0], arr_IK[1,0])
    vec_K = I + v_IK
    K = Point(vec_K.x, vec_K.y, name="K")
    
    # L 
    v_IL = v_IK.to_unit() * d_IL
    vec_L = I + v_IL
    L = Point(vec_L.x, vec_L.y, name="L")

    return [A, B, C, D, E, F, G, H, I, J, K, L]

def get_lines5(points):
    A, B, C, D, E, F, G, H, I, J, K, L = points
    v_AB = B - A
    v_BC = C - B
    v_CE = E - C
    v_CD = D - C
    v_DJ = J - D
    v_EG = G - E
    v_FG = G - F
    v_GH = H - G
    v_HI = I - H
    v_IJ = J - I 
    v_IK = K - I
    v_JK = K - J
    v_KL = L - K 
    l_AB = Line(A, v_AB, "AB")
    l_BC = Line(B, v_BC, "BC")
    l_CE = Line(C, v_CE, "CE")
    l_CD = Line(C, v_CD, "CD")
    l_DJ = Line(D, v_DJ, "DJ")
    l_EG = Line(E, v_EG, "EG")
    l_FG = Line(F, v_FG, "FG")
    l_GH = Line(G, v_GH, "GH")
    l_HI = Line(H, v_HI, "HI")
    l_IJ = Line(I, v_IJ, "IJ")
    l_IK = Line(I, v_IK, "IK")
    l_JK = Line(J, v_JK, "JK")
    l_KL = Line(K, v_KL, "KL")
    return [l_AB, l_BC, l_CE, l_CD, l_DJ, l_EG, l_FG, l_GH,
            l_HI, l_IJ, l_IK, l_JK, l_KL]


# %% 
if __name__ == "__main__":
    
    A_x = 20
    A_y = 0
    D_x = 79.2
    D_y = 57.6
    d_AB = 12.5
    d_BC = 64
    d_BF = 24
    d_CD = 40
    d_CE = 104
    d_EG = 40
    d_EH = 112
    d_FG = 136

    n_steps = 5
    results = [calculate_linkage(phi, A_x, A_y, D_x, D_y, 
                                 d_AB, d_BC, d_BF, d_CD, 
                                 d_CE, d_EG, d_EH, d_FG) 
                for phi in np.linspace(0, 360, num=n_steps, endpoint=False)]
    
    data = np.empty((n_steps,16))
    for i, points in enumerate(results):
        data[i] = np.concatenate([(p.x, p.y) for p in points])
        
    data = data / 1000
    point_names = ["A", "B", "C", "D", "E", "F", "G", "H"]
    idx = pd.MultiIndex.from_product([point_names, ["x", "y"]])
    df = pd.DataFrame(data, columns=idx)
    
    import matplotlib.pyplot as plt
    point_names = ["A", "B", "C", "D", "E", "F", "G", "H"]
    plt.close("all")
    plt.figure()
    ax = plt.gca()
    ax.set_aspect("equal")
    for p_name in point_names:
        ax.scatter(df[p_name, "x"], df[p_name, "y"])
        
# %% 

    A_x = 20
    A_y = 0
    D_x = 79.2
    D_y = 57.6
    d_AB = 12.5
    d_AF = 12.5
    d_BC = 64
    d_CD = 40
    d_CE = 104
    d_EG = 40
    d_EH = 112
    d_FG = 136

    n_steps = 5
    results = [calculate_linkage2(phi, A_x, A_y, D_x, D_y, 
                                 d_AB, d_AF, d_BC, d_CD, 
                                 d_CE, d_EG, d_EH, d_FG) 
                for phi in np.linspace(0, 360, num=n_steps, endpoint=False)]
    
    data = np.empty((n_steps,16))
    for i, points in enumerate(results):
        data[i] = np.concatenate([(p.x, p.y) for p in points])
        
    data = data / 1000
    point_names = ["A", "B", "C", "D", "E", "F", "G", "H"]
    idx = pd.MultiIndex.from_product([point_names, ["x", "y"]])
    df = pd.DataFrame(data, columns=idx)
    
    import matplotlib.pyplot as plt
    point_names = ["A", "B", "C", "D", "E", "F", "G", "H"]
    plt.close("all")
    plt.figure()
    ax = plt.gca()
    ax.set_aspect("equal")
    for p_name in point_names:
        ax.scatter(df[p_name, "x"], df[p_name, "y"])
        
# %% linkage 3

    A_x = 0
    A_y = 0
    D_x = 20
    D_y = 120
    d_AB = 35
    d_AF = 35
    d_BC = 105
    d_CD = 55
    d_DE = 55
    d_EG = 32
    d_EH = 225
    d_FI = 160
    d_GI = 10

    n_steps = 5
    results = [calculate_linkage3(phi, A_x, A_y, D_x, D_y, 
                                 d_AB, d_AF, d_BC, d_CD, 
                                 d_DE, d_EG, d_EH, d_FI,
                                 d_GI)
                for phi in np.linspace(0, 360, num=n_steps, endpoint=False)]
    
    data = np.empty((n_steps,18))
    for i, points in enumerate(results):
        data[i] = np.concatenate([(p.x, p.y) for p in points])
        
    data = data / 1000
    point_names = ["A", "B", "C", "D", "E", "F", "G", "H", "I"]
    idx = pd.MultiIndex.from_product([point_names, ["x", "y"]])
    df = pd.DataFrame(data, columns=idx)
    
    import matplotlib.pyplot as plt
    plt.close("all")
    plt.figure()
    ax = plt.gca()
    ax.set_aspect("equal")
    for p_name in point_names:
        ax.scatter(df[p_name, "x"], df[p_name, "y"])
    ax.legend(point_names)
    
# %% linkage 4

    A_x = 0
    A_y = 0
    D_x = 20
    D_y = 120
    d_AB = 35
    d_AF = 35
    d_BC = 105
    d_CD = 55
    d_DE = 55
    d_EG = 32
    d_EH = 225
    d_FI = 160
    d_GI = 10
    a_F = 180

    n_steps = 5
    results = [calculate_linkage4(phi, A_x, A_y, D_x, D_y, 
                                 d_AB, d_AF, d_BC, d_CD, 
                                 d_DE, d_EG, d_EH, d_FI,
                                 d_GI, a_F)
                for phi in np.linspace(0, 360, num=n_steps, endpoint=False)]
    
    data = np.empty((n_steps,18))
    for i, points in enumerate(results):
        data[i] = np.concatenate([(p.x, p.y) for p in points])
        
    data = data / 1000
    point_names = ["A", "B", "C", "D", "E", "F", "G", "H", "I"]
    idx = pd.MultiIndex.from_product([point_names, ["x", "y"]])
    df = pd.DataFrame(data, columns=idx)
    
    import matplotlib.pyplot as plt
    plt.close("all")
    plt.figure()
    ax = plt.gca()
    ax.set_aspect("equal")
    for p_name in point_names:
        ax.scatter(df[p_name, "x"], df[p_name, "y"])
    ax.legend(point_names)
    
# %% linkage 5
    
    A_x, A_y = (0, 0)
    F_x, F_y = (0, 50)
    d_AB, d_BC, d_BE, d_CD = (22.5, 35, 50, 5)
    d_EG, d_FG, d_GH, d_HI = (30, 10, 10, 55)
    d_IJ, d_IK, d_IL, d_JK = (15, 25, 279, 20)
    d_DJ = 90

    n_steps = 5
    results = [calculate_linkage5(phi, A_x, A_y, F_x, F_y,
                                   d_AB, d_BC, d_BE, d_CD,
                                   d_EG, d_FG, d_GH, d_HI,
                                   d_IJ, d_IK, d_IL, d_JK,
                                   d_DJ)
                for phi in np.linspace(0, 360, num=n_steps, endpoint=False)]
    
    data = np.empty((n_steps,24))
    for i, points in enumerate(results):
        data[i] = np.concatenate([(p.x, p.y) for p in points])
        
    data = data / 1000
    point_names = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L"]
    idx = pd.MultiIndex.from_product([point_names, ["x", "y"]])
    df = pd.DataFrame(data, columns=idx)
    
    import matplotlib.pyplot as plt
    plt.close("all")
    plt.figure()
    ax = plt.gca()
    ax.set_aspect("equal")
    for p_name in point_names:
        ax.scatter(df[p_name, "x"], df[p_name, "y"])
    ax.legend(point_names)
