import math

import numpy as np
import matplotlib.pyplot as plt

from geometries import Point
 
class SolutionError(RuntimeError):
    """Raised when no or an infinite number of solutions are found."""
        
def calculate_circle_intersections(circle1, circle2):
    # nach http://paulbourke.net/geometry/circlesphere/
    # "Intersection of two circles"
    p0 = circle1.origin
    p1 = circle2.origin
    d = (p1 - p0).norm()
    r0 = circle1.radius
    r1 = circle2.radius
    
    if d > r0 + r1:
        raise SolutionError("no solutions, the circles are separate")
    if d < abs(r0 - r1):
        raise SolutionError("no solutions, one circle is contained within the other.")
    if d == 0 and r0 == r1:
        raise SolutionError("infinite number of solutions, circles are coincident")
        

    a = (r0*r0 - r1*r1 + d*d) / (2*d) 
    h = math.sqrt(r0 * r0 - a * a)
    
    p2 = p0 + (p1 - p0) * (a / d)
    
    x_inter0 = p2.x + h * (p1.y - p0.y) / d
    y_inter0 = p2.y - h * (p1.x - p0.x) / d 
    p_inter0 = Point(x_inter0, y_inter0)
    
    x_inter1 = p2.x - h * (p1.y - p0.y) / d
    y_inter1 = p2.y + h * (p1.x - p0.x) / d
    p_inter1 = Point(x_inter1, y_inter1)
    
    return p_inter0, p_inter1
    
    
    
if __name__ == "__main__":
    from geometries import Point, Vector, Circle, Plotter
    p = Point(1, 2)
    q = Point(2, 5)

    c1 = Circle(p, 5)
    c2 = Circle(q, 3)
    
    plt.close("all")
    plt.figure()
    ax = plt.gca()
    ax.set_aspect("equal")
    
    plotter = Plotter(ax)
    
    plotter.mpl_circles([c1, c2])
#    c1.plot(ax)
#    c2.plot(ax)
#    
    ps1, ps2 = calculate_circle_intersections(c1, c2)
    ps1.name = "PS1"
    plotter.mpl_points([ps1, ps2], color="r")
#    ps1.plot(ax, color="r")
#    ps2.plot(ax, color="r")
    
    

    
    
    
    
    