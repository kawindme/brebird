import pandas as pd

def _load():
    neck = pd.read_csv("Gatesy93_Neck.csv", sep="\t", header=1, 
                       decimal=",")
    left = pd.read_csv("Gatesy93_Left.csv", sep="\t", header=1, 
                       decimal=",")
    right = pd.read_csv("Gatesy93_Right.csv", sep="\t", header=1, 
                        decimal=",")
    
    idx = pd.MultiIndex.from_arrays([["left", "left", "right", "right"],
                                    ["x", "y", "x", "y"]])
    data = pd.concat([left - neck, right - neck], axis=1).drop("t", axis=1)
    data.columns = idx
    data.index = range(0, 140, 20)
    x_means = (data.right.x - data.left.x) / 2
    y_means = (data.right.y + data.left.y) / 2
    return data, x_means[:-1], y_means[:-1]

data, x_means, y_means = _load()

if __name__ == "__main__":
    import matplotlib.pyplot as plt
    
    plt.close("all")
    plt.figure()
    plt.scatter(data.left.x.abs(), data.left.y)
    plt.scatter(data.right.x, data.right.y)
    for t in data.index:
        plt.annotate(str(t), (abs(data.left.x[t]), data.left.y[t]))
        plt.annotate(str(t), (data.right.x[t], data.right.y[t]))
        
    
    plt.figure()
#    plt.scatter(x_means, data.index[:-1])
    plt.scatter(x_means, y_means)