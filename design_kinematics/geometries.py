import math
import numpy as np

class Point:
    
    def __init__(self, x, y, name=None):
        self.x = x
        self.y = y
        self.name = name
        
    def __repr__(self):
        if self.name is not None:
            return "{}(x={}, y={}, name={})".format(self.__class__.__name__, self.x, self.y, self.name)
        else:
            return "{}(x={}, y={})".format(self.__class__.__name__, self.x, self.y)
    
    
    def __add__(self, other):
        return Vector(self.x + other.x, self.y + other.y)
    
    def __sub__(self, other):
        return Vector(self.x - other.x, self.y - other.y)
    
    
class Vector(Point):
        
    def __mul__(self, other):
        return Vector(self.x * other, self.y * other)

    def norm(self):
        return math.sqrt(self.x * self.x + self.y * self.y)
        
    def to_unit(self):
        mag = self.norm()
        return Vector(self.x / mag, self.y / mag)
    
    
class Circle:
    
    def __init__(self, origin, radius):
        self.origin = origin
        self.radius = radius
        
        
class Line:
    
    def __init__(self, origin, vector, name=None):
        self.start = origin
        end_vec = (origin + vector)
        self.end = Point(end_vec.x, end_vec.y)
        self.name = name
        self.x1 = self.start.x
        self.y1 = self.start.y
        self.x2 = self.end.x
        self.y2 = self.end.y
        
        
class Plotter:
    
    def __init__(self, ax):
        self.ax = ax
    
    
    def mpl_points(self, points, color="k"):
        for point in points:
            self.ax.scatter(point.x, point.y, c=color, marker="x")
            if point.name:
                self.ax.annotate(point.name, (point.x, point.y))
        
             
    def mpl_lines(self, lines):
        for line in lines:
            self.ax.plot([line.x1, line.x2], [line.y1, line.y2])
    
    
    def mpl_triangles(self, triangles):
        for tri in triangles:
            tri_lines = [tri.a, tri.b, tri.c]
            self.mpl_lines(tri_lines)
        
        
    def mpl_circles(self, circles):
        phi = np.deg2rad(np.linspace(0, 360, num=100))
        center_points = []
        for circ in circles:
            xs = circ.origin.x + circ.radius * np.cos(phi)
            ys = circ.origin.y + circ.radius * np.sin(phi)
            self.ax.plot(xs, ys)
            center_points.append(circ.origin)
            
        self.mpl_points(center_points)
# %%
            
class Triangle:
    
    def __init__(self, p1, p2, p3):
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3
        self.a = Line(p1, p2 - p1)
        self.b = Line(p2, p3 - p2)
        self.c = Line(p3, p1 - p3)
        self.points = [p1, p2, p3]